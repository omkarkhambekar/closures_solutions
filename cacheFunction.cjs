function cacheFunction(callback){
    if(typeof callback !== 'function'){
        throw new Error("given function is not a correct type to pass");
    }
    const cache = {};
    return function callbackInvoker(...arg) {
        let args = JSON.stringify(arg);
        if(args in cache){
            //console.log("arg is present return from cache");
            //console.log('cache', cache)
            return cache[args];
        }
        else{
            //console.log("arg is not present store it in cache for future");
            // const result = callback(...arg);
            console.log('cache', cache)
            cache[args] = callback(...arg);
            return cache[args];
        }
    };
}
module.exports = cacheFunction;