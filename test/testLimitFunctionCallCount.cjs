const result = require('../limitFunctionCallCount.cjs');

try {

    const callback = result(function (a,b) {
        console.log("Hello from callback");
    });

    console.log(callback('a','b'));
    console.log(callback('a','b'));
    console.log(callback('a','b'));
    console.log(callback('a','b'));    

} catch (error) {

    console.log(error);
}
