const result = require('../cacheFunction.cjs');

function callback(...a) {
    let multiple = 1;
    for(let index = 0; index < a.length; index++){
        multiple *= a[index];
    }
    return multiple;
};
const cache = result(callback);
console.log(cache(1,2,3));
console.log(cache(4,5,6));
console.log(cache(1,2,3));
console.log(cache(7,6,5));
console.log(cache(1,2,3));