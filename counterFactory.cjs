function counterFactory(){
    let count = 0;
    
    return{
        increment : function() {
            count += 1;
            return count;
        },
        decrement : function() {
            count -= 1;
            return count;
        }
    };
}
module.exports = counterFactory;