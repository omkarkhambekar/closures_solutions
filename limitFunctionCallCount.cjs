function limitFunctionCallCount(callback, n) {
    if(typeof callback !== 'function' || typeof n !== 'number'){
        throw new Error("this is invalid input type");
    }
    let count = 0;
    return function limitFunction(...args) {
        if(count < n){
            count++;
            return callback(...args);
        }
        else{
            return null;
        }
    }
}	
module.exports = limitFunctionCallCount;
